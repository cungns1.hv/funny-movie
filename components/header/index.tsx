import { useRouter } from "next/router";
const Header: React.FC = () => {
  const router = useRouter();
  return (
    <header className="py-20 border-bottom bd-w-3">
      <div className="container d-flex justify-content-between">
        <h1
          className="fw-700 pointer text-white"
          onClick={() => {
            router.push("/");
          }}
        >
          Funny Video
        </h1>
        <div>
          <button
            className="btn btn-primary px-30 mr-10"
            onClick={() => {
              router.push("/login");
            }}
          >
            Login
          </button>
          <button
            className="btn btn-primary px-30"
            onClick={() => router.push("/register")}
          >
            Register
          </button>
        </div>
      </div>
    </header>
  );
};

export default Header;
