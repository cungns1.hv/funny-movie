const withImages = require("next-images");
module.exports = withImages({
  createError(message, config, code, request, response) {
    var error = new Error(message);
    return enhanceError(error, config, code, request, response);
  },
  distDir: "_next",
  images: {
    disableStaticImages: true,
  },
});
