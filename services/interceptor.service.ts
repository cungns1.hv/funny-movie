import axios from "axios";

const Interceptor = () => {
  axios.defaults.baseURL = process.env.NEXT_PUBLIC_BASE_API;
  axios.interceptors.request.use(
    function (config) {
      const token = localStorage.getItem("token");
      if (token) {
        config.headers.Authorization = `Bearer ${token}`;
      }
      return config;
    },
    function (error) {
      return Promise.reject(error);
    }
  );
};

export default Interceptor;
