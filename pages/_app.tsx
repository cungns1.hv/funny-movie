import "aos/dist/aos.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.scss";
import Head from "next/head";
import Interceptor from "../services/interceptor.service";
import { Provider } from "react-redux";
import type { AppProps } from "next/app";
import { store } from "../redux/store";

import AOS from "aos";

import { useEffect } from "react";

function MyApp({ Component, pageProps }: AppProps) {
  Interceptor();
  useEffect(() => {
    AOS.init({
      easing: "ease-out-cubic",
      offset: 250,
      duration: 1000,
      once: true,
    });
  }, []);
  return (
    <>
      <Head>
        <title>Funny Video</title>
        <meta name="description" content="funny video" />
        <link rel="icon" href="./vercel.svg" />
      </Head>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </>
  );
}
export default MyApp;
