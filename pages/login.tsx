import { css } from "@emotion/react";
import { Form, Button } from "react-bootstrap";
import MainLayout from "./main-layout";
import { useRouter } from "next/router";
const Login: React.FC = () => {
  const router = useRouter();
  return (
    <MainLayout>
      <div
        className="d-flex flex-column justify-content-center"
        css={css`
          width: 500px;
          margin: 0 auto;
          min-height: calc(100vh - 95px);
        `}
      >
        <h3 className="text-left mb-20 fz-30 text-white">Login</h3>
        <Form className="border radius-5 p-20 w-100 bd-w-2 bg-white">
          <Form.Group className="mb-20" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" />
          </Form.Group>

          <Form.Group className="mb-20" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" />
          </Form.Group>

          <Button variant="primary" type="submit" className="px-30">
            Login
          </Button>
          <div className="mt-10">
            <span>Don't have a accout</span>{" "}
            <a
              className="ml-5 pointer"
              onClick={() => router.push("/register")}
            >
              sign up
            </a>
          </div>
        </Form>
      </div>
    </MainLayout>
  );
};

export default Login;
