import React from "react";
import Header from "../components/header";

export default function MainLayout(props) {
  const { children } = props;
  return (
    <>
      <div className="tech-bg mh-100vh">
        <Header></Header>
        <main>{children}</main>
      </div>
    </>
  );
}
